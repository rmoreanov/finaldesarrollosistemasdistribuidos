class SolicitudesRecibidas extends React.Component {
    constructor(props) {
        super(props);
    }
    onClickAceptar = () => {
        fetch("http://127.0.0.1:8080/api/invitacion", {
            method: "PUT",
            headers: {
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.props.invitacion.invitacionId)
        }).then(function (response) {
            return Promise.all([response.ok, response.json()])
        }).then(function ([responseOk, data]) {
            console.clear();
            console.log(data);
            if(responseOk){
                alert("Invitación aceptada correctamente.");
                window.location.reload();
            }
        })
    }
    onClickCancelar = () => {
        fetch("http://127.0.0.1:8080/api/invitacion", {
            method: "DELETE",
            headers: {
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.props.invitacion.invitacionId)
        }).then(function (response) {
            return Promise.all([response.ok, response.json()])
        }).then(function ([responseOk, data]) {
            console.clear();
            console.log(data);
            if(responseOk){
                alert("Invitación eliminada correctamente.");
                window.location.reload();
            }
        })
    }
    render() {
        const cardUsuarioStyle = {
            marginBottom: "10px",
            padding: "10px"
        }
        const photoUsuarioStyle = {
            width: "45px",
            height: "45px"
        }
        const photoRestauranteStyle = {
            width: "68px",
            height: "45px"
        }
        const usuarioDatosStyle = {
            marginLeft: "10px"
        }
        const nicknameUsuarioStyle = {
            margin: "0px"
        }
        const fechaInvitacionStyle = {
            marginBottom: "5px",
            marginTop: "5px"
        }
        const cancelarInvitacionStyle = {
            marginLeft: "5px",
        }
        const aceptadaInvitacionStyle = {
            marginTop: "-5px",
            marginBottom: "-5px"
        }
        return (
            <div className="card" style={cardUsuarioStyle}>
                <div className="d-flex flex-row">
                    <div className="d-flex align-items-center w-50">
                        <div className="d-flex">
                            <img className="rounded" width="100%" src={"img/photos/"+(this.props.imageServer ? this.props.invitacion.usuario.codigo : 0)+".jpg"} style={photoUsuarioStyle}/>
                        </div>
                        <div className="d-flex flex-column" style={usuarioDatosStyle}>
                            <div className="d-flex">
                                <h6 className="text-left" style={nicknameUsuarioStyle}>{this.props.invitacion.usuario.nickname}</h6>
                            </div>
                            <div className="d-flex">
                                <small className="text-left">{this.props.invitacion.usuario.correo}</small>
                            </div>
                        </div>
                    </div>
                    <div className="d-flex align-items-center">
                        <div className="d-flex">
                            <img className="rounded" width="100%" src={"img/restaurants/"+(this.props.imageServer ? this.props.invitacion.restaurante.codigo : 0)+".jpg"} style={photoRestauranteStyle}/>
                        </div>
                        <div className="d-flex flex-column" style={usuarioDatosStyle}>
                            <div className="d-flex">
                                <h6 className="text-left" style={nicknameUsuarioStyle}>{this.props.invitacion.restaurante.razonSocial}</h6>
                            </div>
                            <div className="d-flex">
                                <small className="text-left">{this.props.invitacion.restaurante.provincia+" - "+this.props.invitacion.restaurante.direccion}</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="d-flex">
                    <h6 style={fechaInvitacionStyle}>
                        {"Fecha: "}
                        <small>{this.props.invitacion.date.replace("T", " ")}</small>
                    </h6>
                </div>
                <div className="d-flex">
                    {
                        this.props.invitacion.estado=="pendiente" ? (
                            <div>
                                <button className="btn btn-outline-primary btn-sm" onClick={this.onClickAceptar}>
                                    Aceptar Invitación
                                </button>
                                <button className="btn btn-outline-danger btn-sm" style={cancelarInvitacionStyle} onClick={this.onClickCancelar}>
                                    Cancelar Invitación
                                </button>
                            </div>
                        ) : (
                            <p className="text-success" style={aceptadaInvitacionStyle}>Invitación Aceptada</p>
                        )
                    }
                </div>
            </div>
        )
    }
}