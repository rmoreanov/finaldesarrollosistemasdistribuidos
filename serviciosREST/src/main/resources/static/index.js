'use strict';

const Link = ReactRouterDOM.Link
const Route = ReactRouterDOM.Route

const App = props => (
  <ReactRouterDOM.HashRouter>
    <Route path="/" exact component={Login} />
    <Route path="/registrar" exact component={Signin} />
    <Route path="/usuario/:codigo" exact component={Usuario} />
    <Route path="/usuario/:codigo/invitar/:codigoInvitado" exact component={Invitacion} />
  </ReactRouterDOM.HashRouter>
)

ReactDOM.render(<App />, document.querySelector('#root'));