class RestauranteCercano extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        form: {
            date: new Date().toISOString().slice(0, -8)
        }
    };
    }
    onSocialClick = (url) => {
        window.open(url, '_blank');
    }
    onClick = () => {
        const { history, match } = this.props;
        var form = {
            "usuario": {"codigo": this.props.match.params.codigo},
            "usuarioInvitado": {"codigo": this.props.match.params.codigoInvitado},
            "restaurante": {"codigo": this.props.restaurante.codigo},
            "date": this.state.form.date,
            "estado": "pendiente"
        }
        fetch("http://127.0.0.1:8080/api/invitacion", {
            method: "POST",
            headers: {
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(form)
        }).then(function (response) {
            return Promise.all([response.ok, response.json()])
        }).then(function ([responseOk, data]) {
            console.clear();
            console.log(data);
            if(responseOk){
                alert("Invitación enviada con éxito.")
                history.push('/usuario/'+match.params.codigo)
            }
        })
    }
    handleInputChange = (event) => {
        const { name, value } = event.target;
        this.setState(prevState => ({
                form: {
                    ...prevState.form,
                    [name]: value
                }
            })
        );
    }
    render() {
        const cardUsuarioStyle = {
            marginBottom: "10px",
            padding: "10px"
        }
        const usuarioStyle = {
            marginBottom: "10px"
        }
        const photoRestauranteStyle = {
            width: "150px",
            height: "100px"
        }
        const usuarioDatosStyle = {
            marginLeft: "10px"
        }
        const btnSocialStyle = {
            borderColor: "gray",
            width: "35px",
            height: "35px",
            padding: "0px",
            marginLeft: "2.5px"
        }
        const iconSocialStyle = {
            fontSize: "20px",
            color: "gray"
        }
        const invitarUsuarioStyle = {
            width: "200px",
            marginTop: "10px"
        }
        const dateUsuarioStyle = {
            width: "200px",
            fontSize: "14px"
        }
        return (
            <div className="card" style={cardUsuarioStyle}>
                <div className="d-flex align-items-center" style={usuarioStyle}>
                    <div className="d-flex">
                        <img className="rounded" width="100%" src={"img/restaurants/"+(this.state.imageServer ? this.props.restaurante.codigo : 0)+".jpg"} style={photoRestauranteStyle}/>
                    </div>
                    <div className="d-flex flex-column" style={usuarioDatosStyle}>
                        <div className="d-flex">
                            <h5 className="text-left">{this.props.restaurante.razonSocial}</h5>
                        </div>
                        <div className="d-flex">
                            <small className="text-left">{this.props.restaurante.provincia+" - "+this.props.restaurante.direccion}</small>
                        </div>
                    </div>
                </div>
                <div className="d-flex">
                    <input className="form-control" type="datetime-local" name="date" value={this.state.form.date} style={dateUsuarioStyle} onChange={this.handleInputChange}/>
                </div>
                <div className="d-flex">
                    <button className="btn btn-outline-primary btn-sm" type="button" style={invitarUsuarioStyle} onClick={this.onClick}>
                        <i className="icon ion-ios-paper-plane"></i>
                        <span> Enviar Invitación</span>
                    </button>
                    <div className="d-flex w-100 justify-content-end">
                        <div className="d-flex">
                            <button className="btn btn-outline-light" style={btnSocialStyle} onClick={() => this.onSocialClick("https://www.facebook.com/")}>
                                <i className="icon ion-logo-facebook" style={iconSocialStyle}></i>
                            </button>
                        </div>
                        <div className="d-flex">
                            <button className="btn btn-outline-light" style={btnSocialStyle} onClick={() => this.onSocialClick("https://www.instagram.com/")}>
                                <i className="icon ion-logo-instagram" style={iconSocialStyle}></i>
                            </button>
                        </div>
                        <div className="d-flex">
                            <button className="btn btn-outline-light" style={btnSocialStyle} onClick={() => this.onSocialClick("https://www.google.com.pe/maps")}>
                                <i className="icon ion-ios-pin" style={iconSocialStyle}></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}