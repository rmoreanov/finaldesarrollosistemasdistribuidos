class Usuario extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            usuario: {
                codigo: "0",
                contrasenna: "",
                correo: "",
                latitud: "",
                longitud: "",
                nickname: "",
                sexo: "",
                invitaciones: []
            },
            usuariosCercanos: [],
            imageServer: false,
            invitacionesEnviadas: [],
            invitacionesInvitadores: [],
            form: {
                radio: "3.25",
                sexo: "M"
            }
        };
    }
    componentDidMount() {
        const self = this
        self.getImageServer()
    }
    getImageServer = () => {
        const self = this
        fetch("http://127.0.0.1:8080/api/imageServer")
        .then(function (response) {
            return Promise.all([response.ok, response.json()])
        }).then(function ([responseOk, data]) {
            if(responseOk){
                console.clear();
                self.setState({
                    imageServer: true
                }, function() {
                    self.getUsuario()
                });
            }else {
                self.setState({
                    imageServer: false
                },function() {
                    self.getUsuario()
                })
            }
            console.log(data);
        })
    }
    getUsuario = () => {
        const self = this
        fetch("http://127.0.0.1:8080/api/usuario"+"?codigo="+this.props.match.params.codigo)
        .then(function (response) {
            return Promise.all([response.ok, response.json()])
        }).then(function ([responseOk, data]) {
            if(responseOk){
                self.setState(prevState => ({
                    usuario: data,
                    form: {
                        ...prevState.form,
                        sexo: data.sexo=="H"?"M":"H"
                    }
                }), function() {
                    self.getInvitacionesEnviadas()
                });
            }
            console.log(data);
        })
    }
    getInvitacionesEnviadas = () => {
        const self = this
        fetch("http://127.0.0.1:8080/api/invitacionesEnviadas"+"?codigo="+this.props.match.params.codigo)
        .then(function (response) {
            return Promise.all([response.ok, response.json()])
        }).then(function ([responseOk, data]) {
            if(responseOk){
                self.setState({
                    invitacionesEnviadas: data
                }, function() {
                    self.getInvitacionesInvitadores()
                });
            }
            console.log(data);
        })
    }
    getInvitacionesInvitadores = () => {
        const self = this
        fetch("http://127.0.0.1:8080/api/invitacionesInvitadores"+"?codigo="+this.props.match.params.codigo)
        .then(function (response) {
            return Promise.all([response.ok, response.json()])
        }).then(function ([responseOk, data]) {
            if(responseOk){
                self.setState({
                    invitacionesInvitadores: data
                }, function() {
                    $('#buttonBuscar').trigger('click')
                });
            }
            console.log(data);
        })
    }
    onSocialClick = (url) => {
        window.open(url, '_blank');
    }
    onClick = () => {
        const self = this
        fetch("http://127.0.0.1:8080/api/usuariosradio"+"?codigo="+this.state.usuario.codigo+"&radio="+this.state.form.radio+"&sexo="+this.state.form.sexo)
        .then(function (response) {
            return Promise.all([response.ok, response.json()])
        }).then(function ([responseOk, data]) {
            self.setState({
                usuariosCercanos: data
            })
            console.log(data);
        })
    }
    handleInputChange = (event) => {
        const { name, value } = event.target;
        this.setState(prevState => ({
                form: {
                    ...prevState.form,
                    [name]: value
                }
            })
        );
    }
    esInvitadoOEstaInvitado = (usuario) => {
        var esInvitado = false;
        for(var i=0;i<this.state.usuario.invitaciones.length;i++){
            if(usuario.codigo==this.state.usuario.invitaciones[i].invitacionId.usuarioInvitadoId){
                esInvitado = true;
                break;
            }
        }
        for(var i=0;i<this.state.invitacionesInvitadores.length;i++){
            if(usuario.codigo==this.state.invitacionesInvitadores[i].invitacionId.usuarioId){
                esInvitado = true;
                break;
            }
        }
        return esInvitado
    }
    render() {
        const divUsuarioStyle = {
            width: "500px",
            margin: "0 auto",
            padding: "10px",
        }
        const usuarioStyle = {
            marginBottom: "10px"
        }
        const photoUsuarioStyle = {
            width: "100px",
            height: "100px"
        }
        const usuarioDatosStyle = {
            marginLeft: "10px"
        }
        const formUsuarioStyle = {
            margin: "10px"
        }
        const inputUsuarioStyle = {
            margin: "2.5px"
        }
        const btnSocialStyle = {
            borderColor: "gray",
            width: "35px",
            height: "35px",
            padding: "0px",
            margin: "2.5px"
        }
        const iconSocialStyle = {
            fontSize: "20px",
            color: "gray"
        }
        const btnUsuarioStyle = {
            padding: "3px 10px"
        }
        const iconUsuarioStyle = {
            fontSize: "20px"
        }
        const kmUsuarioStyle = {
            marginTop: "10px"
            
        }
        return (
            <div className="d-flex flex-column" style={divUsuarioStyle}>
                <div className="d-flex align-items-center" style={usuarioStyle}>
                    <div className="d-flex">
                        <img className="rounded" width="100%" src={"img/photos/"+ (this.state.imageServer ? this.state.usuario.codigo : 0) +".jpg"} style={photoUsuarioStyle}/>
                    </div>
                    <div className="d-flex flex-column" style={usuarioDatosStyle}>
                        <div className="d-flex">
                            <h2 className="text-left">{this.state.usuario.nickname}</h2>
                        </div>
                        <div className="d-flex">
                            <h6 className="text-left">{this.state.usuario.correo}</h6>
                        </div>
                    </div>
                    <div className="d-flex w-100 justify-content-end">
                        <div className="d-flex flex-column">
                            <div className="d-flex">
                                <button className="btn btn-outline-light" style={btnSocialStyle} onClick={() => this.onSocialClick("https://www.facebook.com/")}>
                                    <i className="icon ion-logo-facebook" style={iconSocialStyle}></i>
                                </button>
                            </div>
                            <div className="d-flex">
                                <button className="btn btn-outline-light" style={btnSocialStyle} onClick={() => this.onSocialClick("https://twitter.com/")}>
                                    <i className="icon ion-logo-twitter" style={iconSocialStyle}></i>
                                </button>
                            </div>
                            
                            <div className="d-flex">
                                <button className="btn btn-outline-light" style={btnSocialStyle} onClick={() => this.onSocialClick("https://www.instagram.com/")}>
                                    <i className="icon ion-logo-instagram" style={iconSocialStyle}></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.invitacionesEnviadas.length > 0 &&
                    <div className="d-flex flex-column">
                        <div className="d-flex">
                            <h6>Invitaciones Enviadas</h6>
                        </div>
                        {
                            this.state.invitacionesEnviadas.map((invitacion, index) =>
                                <SolicitudesEnviadas key={index} imageServer={this.state.imageServer} invitacion={invitacion}/>
                            )
                        }
                    </div>
                }
                {
                    this.state.invitacionesInvitadores.length > 0 &&
                    <div className="d-flex flex-column">
                        <div className="d-flex">
                            <h6>Invitaciones Recibidas</h6>
                        </div>
                        {
                            this.state.invitacionesInvitadores.map((invitacion, index) =>
                                <SolicitudesRecibidas key={index} imageServer={this.state.imageServer} invitacion={invitacion}/>
                            )
                        }
                    </div>
                }                
                <div className="d-flex justify-content-center">
                    <form className="form-inline" method="post" style={formUsuarioStyle}>
                        <div className="form-group" style={inputUsuarioStyle}>
                            <input className="form-control d-inline-flex" type="number" name="radio" placeholder="Radio" min="0" max="10" step="0.25" value={this.state.form.radio} onChange={this.handleInputChange}/>
                        </div>
                        <div className="form-group" style={inputUsuarioStyle}>
                            <h6 className="d-inline-flex" style={kmUsuarioStyle}>km</h6>
                        </div>
                        <div className="form-group" style={inputUsuarioStyle}>
                            <select className="form-control" name="sexo" value={this.state.form.sexo} onChange={this.handleInputChange}>
                                <option value="">Sexo</option>
                                <option value="H">Hombre</option>
                                <option value="M">Mujer</option>
                                <option value="HM">Ambos</option>
                            </select>
                        </div>
                        <div className="form-group" style={inputUsuarioStyle}>
                            <button id="buttonBuscar" className="btn btn-primary" type="button" style={btnUsuarioStyle} onClick={this.onClick}>
                                <i className="icon ion-ios-search" style={iconUsuarioStyle}></i>
                            </button>
                        </div>
                    </form>
                </div>
                <div className="d-flex flex-column">
                    {
                        this.state.usuariosCercanos.map((usuario, index) =>
                            <UsuarioCercano key={index} imageServer={this.state.imageServer} history={this.props.history} match={this.props.match} usuario={usuario} estado={this.esInvitadoOEstaInvitado(usuario)}/>
                        )
                    }
                </div>
            </div>
        )
    }
}

class UsuarioCercano extends React.Component {
    constructor(props) {
        super(props);
    }
    onSocialClick = (url) => {
        window.open(url, '_blank');
    }
    onClick = () => {
        const { history } = this.props;
        if(!this.props.estado){
            history.push('/usuario/'+this.props.match.params.codigo+'/invitar/'+this.props.usuario.codigo)
        }
    }
    render() {
        const cardUsuarioStyle = {
            marginBottom: "10px",
            padding: "10px"
        }
        const usuarioStyle = {
            marginBottom: "10px"
        }
        const photoUsuarioStyle = {
            width: "100px",
            height: "100px"
        }
        const usuarioDatosStyle = {
            marginLeft: "10px"
        }
        const btnSocialStyle = {
            borderColor: "gray",
            width: "35px",
            height: "35px",
            padding: "0px",
            marginLeft: "2.5px"
        }
        const iconSocialStyle = {
            fontSize: "20px",
            color: "gray"
        }
        const invitarUsuarioStyle = {
            width: "200px",
            filter :  this.props.estado ? "grayscale(100%)" : "grayscale(0%)"
        }
        return (
            <div className="card" style={cardUsuarioStyle}>
                <div className="d-flex align-items-center" style={usuarioStyle}>
                    <div className="d-flex">
                        <img className="rounded" width="100%" src={"img/photos/"+(this.props.imageServer ? this.props.usuario.codigo : 0)+".jpg"} style={photoUsuarioStyle}/>
                    </div>
                    <div className="d-flex flex-column" style={usuarioDatosStyle}>
                        <div className="d-flex">
                            <h2 className="text-left">{this.props.usuario.nickname}</h2>
                        </div>
                        <div className="d-flex">
                            <h6 className="text-left">{this.props.usuario.correo}</h6>
                        </div>
                    </div>
                </div>
                <div className="d-flex">
                    <button className="btn btn-outline-primary btn-sm" type="button" style={invitarUsuarioStyle} onClick={this.onClick} disabled={this.props.estado}>
                        <i className="icon ion-ios-paper-plane"></i>
                        <span> Invitar a comer</span>
                    </button>
                    <div className="d-flex w-100 justify-content-end">
                        <div className="d-flex">
                            <button className="btn btn-outline-light" style={btnSocialStyle} onClick={() => this.onSocialClick("https://www.facebook.com/")}>
                                <i className="icon ion-logo-facebook" style={iconSocialStyle}></i>
                            </button>
                        </div>
                        <div className="d-flex">
                            <button className="btn btn-outline-light" style={btnSocialStyle} onClick={() => this.onSocialClick("https://twitter.com/")}>
                                <i className="icon ion-logo-twitter" style={iconSocialStyle}></i>
                            </button>
                        </div>
                        <div className="d-flex">
                            <button className="btn btn-outline-light" style={btnSocialStyle} onClick={() => this.onSocialClick("https://www.instagram.com/")}>
                                <i className="icon ion-logo-instagram" style={iconSocialStyle}></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}