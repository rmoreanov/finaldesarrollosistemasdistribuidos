class Invitacion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            usuario: {
                codigo: "0",
                contrasenna: "",
                correo: "",
                latitud: "",
                longitud: "",
                nickname: "",
                sexo: ""
            },
            usuarioInvitado: {
                codigo: "0"
            },
            imageServer: false,
            restaurantesCercanos: [],
            form: {
                radio: "3.25"
            }
        };
    }
    componentDidMount() {
        const self = this
        self.getImageServer()
    }
    getImageServer = () => {
        const self = this
        fetch("http://127.0.0.1:8080/api/imageServer")
        .then(function (response) {
            return Promise.all([response.ok, response.json()])
        }).then(function ([responseOk, data]) {
            if(responseOk){
                console.clear();
                self.setState({
                    imageServer: true
                }, function() {
                    self.getUsuario()
                });
            }else {
                self.setState({
                    imageServer: false
                },function() {
                    self.getUsuario()
                })
            }
            console.log(data);
        })
    }
    getUsuario = () => {
        const self = this
        fetch("http://127.0.0.1:8080/api/usuario"+"?codigo="+this.props.match.params.codigo)
        .then(function (response) {
            return Promise.all([response.ok, response.json()])
        }).then(function ([responseOk, data]) {
            if(responseOk){
                self.setState({
                    usuario: data
                }, function() {
                    self.getUsuarioInvitado()
                    $('#buttonBuscar').trigger('click')
                });
            }
            console.log(data);
        })
    }
    getUsuarioInvitado = () => {
        const self = this
        fetch("http://127.0.0.1:8080/api/usuario"+"?codigo="+this.props.match.params.codigoInvitado)
        .then(function (response) {
            return Promise.all([response.ok, response.json()])
        }).then(function ([responseOk, data]) {
            if(responseOk){
                self.setState({
                    usuarioInvitado: data
                });
            }
            console.log(data);
        })
    }
    onSocialClick = (url) => {
        window.open(url, '_blank');
    }
    onClick = () => {
        const self = this
        fetch("http://127.0.0.1:8080/api/restaurantesradio"+"?codigo="+this.state.usuario.codigo+"&radio="+this.state.form.radio)
        .then(function (response) {
            return Promise.all([response.ok, response.json()])
        }).then(function ([responseOk, data]) {
            self.setState({
                restaurantesCercanos: data
            })
            console.log(data);
        })
    }
    handleInputChange = (event) => {
        const { name, value } = event.target;
        this.setState(prevState => ({
                form: {
                    ...prevState.form,
                    [name]: value
                }
            })
        );
    }
    render() {
        const divIconEnviarStyle = {
            position: "absolute",
            paddingRight: "150px"
        }
        const iconEnviarStyle = {
            color: "rgb(191,191,191)",
            fontSize: "45px",
            marginTop: "5px",
            marginRight: "5px"
        }
        const nameFormLoginStyle = {
            color: "rgb(244,175,71)"
        };
        const divUsuarioStyle = {
            width: "500px",
            margin: "0 auto",
            padding: "10px",
        }
        const photoUsuarioStyle = {
            width: "100px",
            height: "100px"
        }
        const photoUsuarioInvitadoStyle = {
            width: "60px",
            height: "60px",
            marginTop: "5px"
        }
        const nicknameUsuarioInvitadoStyle = {
            fontSize: "16px",
            marginBottom: "0px"
        }
        const correoUsuarioInvitadoStyle = {
            fontSize: "12px",
            margin: "0px"
        }
        const usuarioDatosStyle = {
            marginLeft: "10px"
        }
        const formUsuarioStyle = {
            margin: "10px"
        }
        const inputUsuarioStyle = {
            margin: "2.5px"
        }
        const btnUsuarioStyle = {
            padding: "3px 10px"
        }
        const iconUsuarioStyle = {
            fontSize: "18px"
        }
        const iconNameUsuarioStyle = {
            fontSize: "16px"
        }
        const kmUsuarioStyle = {
            marginTop: "10px"
            
        }
        return (
            <div className="d-flex flex-column" style={divUsuarioStyle}>
                <div className="d-flex align-self-center">
                    <h1 style={nameFormLoginStyle}>Invitación</h1>
                </div>
                <div className="d-flex flex-row">
                    <div className="d-flex align-items-center">
                        <div className="d-flex">
                            <img className="rounded" width="100%" src={"img/photos/"+(this.state.imageServer ? this.state.usuario.codigo : 0)+".jpg"} style={photoUsuarioStyle}/>
                        </div>
                        <div className="d-flex flex-column" style={usuarioDatosStyle}>
                            <div className="d-flex">
                                <h2 className="text-left">{this.state.usuario.nickname}</h2>
                            </div>
                            <div className="d-flex">
                                <h6 className="text-left">{this.state.usuario.correo}</h6>
                            </div>
                        </div>
                    </div>
                    <div className="d-flex align-items-center flex-column w-100">
                        <div className="d-flex w-100 justify-content-center">
                            <div className="d-flex" style={divIconEnviarStyle}>
                                <i className="icon ion-ios-paper-plane" style={iconEnviarStyle}></i>
                            </div>
                            <div className="d-flex">
                                <img className="rounded-circle" width="100%" src={"img/photos/"+(this.state.imageServer ? this.state.usuarioInvitado.codigo : 0)+".jpg"} style={photoUsuarioInvitadoStyle}/>
                            </div>
                        </div>
                        <div className="d-flex flex-column">
                            <div className="d-flex justify-content-center">
                                <h2 className="text-left" style={nicknameUsuarioInvitadoStyle}>{this.state.usuarioInvitado.nickname}</h2>
                            </div>
                            <div className="d-flex justify-content-center">
                                <h6 className="text-left" style={correoUsuarioInvitadoStyle}>{this.state.usuarioInvitado.correo}</h6>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div className="d-flex justify-content-center">
                    <form className="form-inline" method="post" style={formUsuarioStyle}>
                        <div className="form-group" style={inputUsuarioStyle}>
                            <input className="form-control d-inline-flex" type="number" name="radio" placeholder="Radio" min="0" max="10" step="0.25" value={this.state.form.radio} onChange={this.handleInputChange}/>
                        </div>
                        <div className="form-group" style={inputUsuarioStyle}>
                            <h6 className="d-inline-flex" style={kmUsuarioStyle}>km</h6>
                        </div>
                        <div className="form-group" style={inputUsuarioStyle}>
                            <button id="buttonBuscar" className="btn btn-primary" type="button" style={btnUsuarioStyle} onClick={this.onClick}>
                                <i className="icon ion-ios-search" style={iconUsuarioStyle}></i>
                                <span style={iconNameUsuarioStyle}> Restaurantes</span>
                            </button>
                        </div>
                    </form>
                </div>
                <div className="d-flex flex-column">
                    {
                        this.state.restaurantesCercanos.map((restaurante, index) =>
                            <RestauranteCercano key={index} imageServer={this.state.imageServer} history={this.props.history} match={this.props.match} restaurante={restaurante}/>
                        )
                    }
                </div>
            </div>
        )
    }
}