package com.upc.serviciosREST.repositorio;

import com.upc.serviciosREST.entidades.Invitacion;
import com.upc.serviciosREST.entidades.InvitacionId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvitacionRespositorio extends CrudRepository<Invitacion,Long> {
    @Query("SELECT i FROM Invitacion i WHERE i.invitacionId.usuarioId=:codigo")
    public List<Invitacion> buscarInvitacionesEnviadas(Long codigo);
    @Query("SELECT i FROM Invitacion i WHERE i.invitacionId.usuarioInvitadoId=:codigo")
    public List<Invitacion> buscarInvitacionesInvitadores(Long codigo);
    @Query("SELECT i FROM Invitacion i WHERE i.invitacionId=:invitacionId")
    public Invitacion buscar(InvitacionId invitacionId);
}
