package com.upc.serviciosREST.entidades;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "TP_INVITACION")
public class Invitacion implements java.io.Serializable {
    @EmbeddedId
    private InvitacionId invitacionId = new InvitacionId();
    @ManyToOne
    @MapsId("usuarioId")
    @JoinColumn(name = "usuarioId", nullable = false, insertable = false, updatable = false)
    private Usuario usuario;
    @ManyToOne
    @MapsId("usuarioInvitadoId")
    @JoinColumn(name = "usuarioInvitadoId", nullable = false, insertable = false, updatable = false)
    private Usuario usuarioInvitado;
    @ManyToOne
    @MapsId("restauranteId")
    @JoinColumn(name = "restauranteId", nullable = false, insertable = false, updatable = false)
    private Restaurante restaurante;
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm")
    private LocalDateTime date;
    private String estado;

    public InvitacionId getInvitacionId() {
        return invitacionId;
    }

    public void setInvitacionId(InvitacionId invitacionId) {
        this.invitacionId = invitacionId;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Usuario getUsuarioInvitado() {
        return usuarioInvitado;
    }

    public void setUsuarioInvitado(Usuario usuarioInvitado) {
        this.usuarioInvitado = usuarioInvitado;
    }

    public Restaurante getRestaurante() {
        return restaurante;
    }

    public void setRestaurante(Restaurante restaurante) {
        this.restaurante = restaurante;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
