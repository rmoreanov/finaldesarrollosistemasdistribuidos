package com.upc.serviciosREST.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class InvitacionId implements Serializable {
    @Column(insertable = false, updatable = false)
    private long usuarioId;
    @Column(insertable = false, updatable = false)
    private long usuarioInvitadoId;
    @Column(insertable = false, updatable = false)
    private long restauranteId;

    public long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public long getUsuarioInvitadoId() {
        return usuarioInvitadoId;
    }

    public void setUsuarioInvitadoId(long usuarioInvitadoId) {
        this.usuarioInvitadoId = usuarioInvitadoId;
    }

    public long getRestauranteId() {
        return restauranteId;
    }

    public void setRestauranteId(long restauranteId) {
        this.restauranteId = restauranteId;
    }
}
