package com.upc.serviciosREST.rest;

import com.upc.serviciosREST.entidades.Restaurante;
import com.upc.serviciosREST.negocio.RestauranteNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RestauranteREST {
    @Autowired
    private RestauranteNegocio restauranteNegocio;

    @PostMapping("/restaurante")
    public Restaurante grabar(@RequestBody Restaurante restaurante){
        try{
            return restauranteNegocio.grabar(restaurante);
        }
        catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Datos incorrectos");
        }
    }
    @PostMapping("/restaurantes")
    public List<Restaurante> grabar(@RequestBody List<Restaurante> restaurantes){
        return restauranteNegocio.grabar(restaurantes);
    }
    @GetMapping("/restaurantesradio")
    public List<Restaurante> buscarRestaurantesRadio(Long codigo, double radio){
        return restauranteNegocio.buscarRestaurantesRadio(codigo, radio);
    }
}
