package com.upc.serviciosREST.rest;

import com.upc.serviciosREST.entidades.Invitacion;
import com.upc.serviciosREST.entidades.InvitacionId;
import com.upc.serviciosREST.negocio.InvitacionNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api")
public class InvitacionREST {
    @Autowired
    private InvitacionNegocio invitacionNegocio;

    @PostMapping("/invitacion")
    public Invitacion grabar(@RequestBody Invitacion invitacion){
        try{
            return invitacionNegocio.grabar(invitacion);
        }
        catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Datos incorrectos");
        }
    }
    @GetMapping("/invitacionesEnviadas")
    public List<Invitacion> obtenerInvitacionesEnviadas(Long codigo){
        return invitacionNegocio.buscarInvitacionesEnviadas(codigo);
    }
    @GetMapping("/invitacionesInvitadores")
    public List<Invitacion> obtenerInvitacionesInvitadores(Long codigo){
        return invitacionNegocio.buscarInvitacionesInvitadores(codigo);
    }
    @PutMapping("/invitacion")
    public Invitacion actualizar(@RequestBody InvitacionId invitacionId){
        try {
            return invitacionNegocio.actualizar(invitacionId);
        }
        catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"No es posible actualizar");
        }
    }
    @DeleteMapping("/invitacion")
    public Invitacion borrarInvitacion(@RequestBody InvitacionId invitacionId){
        return invitacionNegocio.borrarInvitacion(invitacionId);
    }
}
