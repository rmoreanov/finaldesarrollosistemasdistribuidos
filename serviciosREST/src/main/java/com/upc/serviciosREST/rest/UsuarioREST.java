package com.upc.serviciosREST.rest;

import com.upc.serviciosREST.entidades.Usuario;
import com.upc.serviciosREST.negocio.UsuarioNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api")

public class UsuarioREST {
    @Autowired
    private UsuarioNegocio usuarioNegocio;

    @PostMapping("/usuario")
    public Usuario grabar(@RequestBody Usuario usuario){
        try{
            return usuarioNegocio.grabar(usuario);
        }
        catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Datos incorrectos");
        }
    }
    @PostMapping("/usuarios")
    public List<Usuario> grabar(@RequestBody List<Usuario> usuarios){
        return usuarioNegocio.grabar(usuarios);
    }
    @PostMapping("/logear")
    public Long obtenerCodigo(@RequestBody Usuario usuario){
        try {
            Usuario usuarioBuscado = usuarioNegocio.buscarUsuario(usuario.getCorreo(),usuario.getContrasenna());
            return usuarioBuscado.getCodigo();
        }
        catch(Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Datos incorrectos para logearse");
        }
    }
    @GetMapping("/usuario")
    public Usuario obtenerUsuarioSinContrasenna(Long codigo){
        return usuarioNegocio.buscarUsuarioSinContrasenna(codigo);
    }
    @GetMapping("/usuariosradio")
    public List<Usuario> buscarUsuariosRadio(Long codigo, double radio, String sexo){
        return usuarioNegocio.buscarUsuariosRadio(codigo, radio, sexo);
    }
}
