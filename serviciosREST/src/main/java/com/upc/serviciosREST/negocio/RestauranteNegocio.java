package com.upc.serviciosREST.negocio;

import com.upc.serviciosREST.entidades.Restaurante;
import com.upc.serviciosREST.entidades.Usuario;
import com.upc.serviciosREST.repositorio.RestauranteRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RestauranteNegocio {

    @Autowired
    private RestauranteRepositorio restauranteRepositorio;
    @Autowired
    private UsuarioNegocio usuarioNegocio;

    public Restaurante grabar(Restaurante restaurante){
        Restaurante restauranteConCoordenadas = new Restaurante();
        restauranteConCoordenadas.setCodigo(restaurante.getCodigo());
        restauranteConCoordenadas.setRuc(restaurante.getRuc());
        restauranteConCoordenadas.setRazonSocial(restaurante.getRazonSocial());
        restauranteConCoordenadas.setDireccion(restaurante.getDireccion());
        restauranteConCoordenadas.setDistrito(restaurante.getDistrito());
        restauranteConCoordenadas.setProvincia(restaurante.getProvincia());
        restauranteConCoordenadas.setTelefono(restaurante.getTelefono());
        restauranteConCoordenadas.setLatitud(crearCoordenadaAleatoria());
        restauranteConCoordenadas.setLongitud(crearCoordenadaAleatoria());
        return restauranteRepositorio.save(restauranteConCoordenadas);
    }
    public List<Restaurante> grabar(List<Restaurante> restaurantes){
        List<Restaurante> restaurantesNuevos = new ArrayList<>();
        for(Restaurante restaurante: restaurantes ){
            restaurantesNuevos.add(grabar(restaurante));
        }
        return restaurantesNuevos;
    }
    public List<Restaurante> buscarRestaurantesRadio(Long codigo, double radio) {
        List<Restaurante> restaurantes = buscarRestaurantes();
        Usuario usuario = usuarioNegocio.buscarUsuarioSinContrasenna(codigo);
        List<Restaurante> restaurantesRadio = new ArrayList<>();
        for(Restaurante restaurante: restaurantes){
            if(calcularDistanciaGeografica(usuario.getLatitud(),usuario.getLongitud(),restaurante.getLatitud(),restaurante.getLongitud())<=radio){
                restaurantesRadio.add(restaurante);
            }
        }
        return restaurantesRadio;
    }

    private double crearCoordenadaAleatoria(){
        return ((Math.random() * ((1d - (-1d)) + 1d)) - 1d)/64d;
    }
    private double calcularDistanciaGeografica(double latitud1, double longitud1, double latitud2, double longitud2){
        double radioTierra = 6378.1d;
        latitud1=latitud1*Math.PI/180;
        longitud1=longitud1*Math.PI/180;
        latitud2=latitud2*Math.PI/180;
        longitud2=longitud2*Math.PI/180;
        double variacionLatitud = latitud2-latitud1;
        double variacionLongitud = longitud2-longitud1;
        double a = Math.pow(Math.sin(variacionLatitud/2),2)+Math.cos(latitud1)*Math.cos(latitud2)*Math.pow(Math.sin(variacionLongitud/2),2);
        double c = 2*Math.atan2(Math.sqrt(a),Math.sqrt(1-a));
        return radioTierra*c;
    }
    private List<Restaurante> buscarRestaurantes(){
        return (List<Restaurante>) restauranteRepositorio.findAll();
    }
}
