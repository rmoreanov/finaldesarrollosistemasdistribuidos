package com.upc.serviciosREST.jms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class JMSProductor {
    @Autowired
    private JmsTemplate jmsTemplate;

    @Value("${jms.cola.envio}")
    private String destinationQueue;

    public void send(String msg) {
        jmsTemplate.convertAndSend(destinationQueue, msg);
    }
}
